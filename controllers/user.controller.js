const { json } = require('body-parser');
const express = require('express');
const userService = require('../services/user.service');
const router = express.Router();

router.post('/register', register)
router.post('/authenticate', authenticate)
router.delete('/:id', _delete)
router.put('/:id', update)
router.get('/:id', getById)

module.exports = router;


function register(req, res, next){
    userService.create(req.body)
        .then(() => res.json({message : "User added"}))
        .catch(err => next(err))
}

function authenticate(req, res, next){
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({message : "Username or password incorrect"}))
        .catch(err => next(err))
}

function update(req, res, next){
    userService.update(req.params.id, req.body)
        .then(() => res.json({message : "User modified"}))
        .catch(err => next(err))
}

function _delete(req, res, next){
    userService.delete(req.params.id)
        .then(() => res.json({message : "User deleted"}))
        .catch(err => next(err))
}

function getById(req, res, next){
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));

}