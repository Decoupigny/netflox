const mongoose = require('mongoose');
const Movie = require('../models/movies');

/**
 * Get all movies
 */
exports.list_all_movies = function(req, res){
    Movie.find({}, function(err, movies){
        res.status(200).json(movies);
    })
}

/**
 * Search by id
 */
exports.movie_by_id = function(req, res, next){
    Movie.find({_id : mongoose.Types.ObjectId(req.params.id)}, function(err, movies){
        res.status(200).json(movies);
    })
}

/**
 * Search by title
 */
exports.movies_by_title = function(req, res, next){
    const title = req.params.title;
    Movie.find({Title : {'$regex': title, '$options': 'i' }}, function(err, movies){
        res.status(200).json(movies);
    })
}
