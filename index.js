const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyPaser = require('body-parser');
const jwt = require('./config/jwt');

const movies_controllers = require('./controllers/movie.controller');
const index_controllers = require('./controllers/index.controller');
const error_controllers = require('./controllers/error.controller');

/**
 * Import .env variables
 */
require('dotenv').config();
const HOSTNAME = process.env.HOST;
const PORT = process.env.PORT;
const MONGO_URL = process.env.MONGO_URL;

app.use(bodyPaser.urlencoded({ extended: false}))
app.use(bodyPaser.json());

// Activate JWT
app.use(jwt());

// Users routes
app.use('/users', require('./controllers/user.controller'));

// Movies routes
app.get('/', index_controllers.index);
app.get('/movies', movies_controllers.list_all_movies);
app.get('/movies/:id', movies_controllers.movie_by_id);
app.get('/movies/title/:title', movies_controllers.movies_by_title);

/**
 * Catch 404 error
 */
app.use(error_controllers.get404);

mongoose.connect(MONGO_URL)
    .then(() => {
        console.log('Mongo connected successfully')
        app.listen(PORT, () => {
            console.log(`Server running on port http://${HOSTNAME}:${PORT}`);
        })
    })
