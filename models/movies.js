const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MovieShema = new Schema({
    Title :{
        type: String,
        required: true
    },
    Year :{
        type: Number,
        required: true
    },
    Summary :{
        type: String,
        required: true
    },
    ShortSummary :{
        type: String,
        required: true
    },
    Genres :{
        type: String,
        required: true
    }, 
    IMDBID :{
        type: String,
        required: true
    }, 
    Runtime :{
        type: Number,
        required: true
    },
    YouTubeTrailer :{
        type: String,
        required: true
    },
    Rating :{
        type: Number,
        required: true
    }, 
    MoviePoster :{
        type: String,
        required: true
    },
    Director :{
        type: String,
        required: true
    },
    Writers:{
        type: String,
        required: true
    },
    Cast:{
        type: String,
        required: true
    }         
});

module.exports = mongoose.model('Movies', MovieShema);