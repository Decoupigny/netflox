const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    Email:{
        type: String,
        required: true
    },
    Password:{
        type: String,
        required: true
    }
})

UserSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret){
        delete ret._id;
        delete ret.Password;
    }
});

module.exports = mongoose.model('User', UserSchema);