const expressJWT = require('express-jwt');
const config = require('./auth.config');
const userService = require('../services/user.service');

function jwt(){
    const secret = config.secret;
    return expressJWT({secret, algorithms: ['HS256']}).unless({
        path:[
            '/users/authenticate',
            'users/register'
        ]
    });
}

module.exports = jwt;