const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/auth.config');
const User = require('../models/users');

module.exports = {
    create,
    authenticate,
    update,
    delete: _delete,
    getById
}

async function create(userParam) {

    // Verification
   if(await User.findOne({ Email: userParam.Email})) {
       throw "Email " + userParam.Email + " is already taken";
   }

   const user = new User(userParam);

   // Hash Password
   if(userParam.Password){
       user.Password = bcrypt.hashSync(userParam.Password, 10);
   }

   // Save
   await user.save();
}

async function getById(id){
    return await User.findById(id);
}

async function authenticate({ Email, Password}){
    const user = await User.findOne({ Email });
    if( user && bcrypt.compareSync(Password, user.Password)){
        const token = jwt.sign({sub : user.id}, config.secret, { expiresIn: '1d'});
        return {
            ...user.toJSON(),
            token
        }
    }
}

async function update(id, userParam){
    const user = await User.findById(id);
    if(!user) throw "User not found";
    if(user.Email !== userParam.Email && await User.findOne({ email: userParam.Email})){
        throw "Email " + userParam.Email + " is already taken";
    }

    if(userParam.password){
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    Object.assign(user, userParam);

    await user.save();

}

async function _delete(id){
    await User.findByIdAndRemove(id);
}